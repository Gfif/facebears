import string
from random import choice

class User:
    def __init__(self, **kwargs):
        for key, val in kwargs.items():
            setattr(self, key, val)

    def dict(self):
        d = {}
        for key, val in vars(self).items():
            if key[0] != "_" and not callable(val):
                d[key] = val
        return d
    def __cmp__(self, c):
        return self.id.__cmp__(c.id)

class Msg:
    def __init__(self, **kwargs):
        for key, val in kwargs.items():
            setattr(self, key, val)

    def dict(self):
        d = {}
        for key, val in vars(self).items():
            if key[0] != "_" and not callable(val):
                d[key] = val
        return d
    def __cmp__(self, c):
        return self.mid.__cmp__(c.mid)



def randstring(n):
    a = string.ascii_letters + string.digits
    return ''.join([choice(a) for i in range(n)])

