#!/usr/bin/env      python
from bottle import *
from hashlib import sha1
import sqlite3
from user import User, randstring, Msg

@route('/')
def index():
    TEMPLATES.clear()
    sid = request.get_cookie("sid")
    if sid is None:
        return template('signin', msg="")
    db = sqlite3.connect("./facebear.db")
    c = db.cursor()
    c.execute('select id from user where sid=?', (sid,))
    ret = c.fetchall()
    db.close()
    if ret == []:
        return template('signin', msg="")
    id = ret[0][0]
    return redirect("/user?id=" + str(id))

@get('/signin')
def login():
    TEMPLATES.clear()
    return template('signin', msg="")
    return redirect("/user?id=" + str(id))

@post('/signin')
def loginpost():
    TEMPLATES.clear()
    db = sqlite3.connect("./facebear.db")
    c = db.cursor()
    name = request.forms.get('name')
    password = request.forms.get('password')
    c.execute('select id, password, sid from user where name=?', (name,))
    ret = c.fetchall()
    if ret == []:
     return template('signin', msg="No such user")
    id, pwdhash, oldsid = ret[0]
    if sha1(password).hexdigest() != pwdhash:
        return template('signin', msg="Incorrect password")
    sid = randstring(10)
    db.execute("update user set sid=? where sid=?;", (sid, oldsid))
    response.set_cookie("sid", sid)
    db.commit()
    db.close()
    return redirect("/user?id=" + str(id))


@get('/user')
def userget():
    TEMPLATES.clear()
    uid = request.query['id']
    
    """Get current user id"""
    db = sqlite3.connect("facebear.db")
    c = db.cursor()
    sid = request.get_cookie("sid")
    c.execute("select id from user where sid=?;", (sid,))
    ret = c.fetchall()
    if ret == []:
        return template('signin', msg="Please, sign in first")
    id = ret[0][0]

    """Requested user information"""
    c.execute("select name, sex, about from user where id= ?;", (uid,))
    ret = c.fetchall()
    if ret == []:
        return template('error', id=id, msg="No such user")
    usr = User(id=uid, name=ret[0][0], sex=ret[0][1], about=ret[0][2])

    """Friends list"""
    c.execute("select id1 from friend where id2=?;", (int(uid),))
    friends = []
    ret = c.fetchall()
    for r in ret:
        c.execute("select name from user where id=?;", (r[0],))
        rret = c.fetchone()
        friends.append(User(id=r[0], name=rret[0]))
    c.execute("select id2 from friend where id1=?;", (int(uid),))
    ret = c.fetchall()
    for r in ret:
        c.execute("select name from user where id=?;", (r[0],))
        rret = c.fetchone()
        friends.append(User(id=r[0], name=rret[0]))
    friends.sort()

    """Is friend???"""
    c.execute("select id from friend where (id1=? and id2=?) or (id1=? and id2=?);", (id, uid, uid, id))
    ret = c.fetchone()
    if ret or id == int(uid):
        isfriend = True
    else:
        isfriend = False

    c.close()
    db.close()
    return template('user', id=id, msg=None, user=usr, isfriend=isfriend, friends=friends)

@get('/add')
def addget():
    TEMPLATES.clear()
    uid = request.query['id']
    db = sqlite3.connect("facebear.db")
    c = db.cursor()
    sid = request.get_cookie("sid")
    c.execute("select id from user where sid=?;", (sid,))
    ret = c.fetchall()
    if ret == []:
        return template('signin', msg="Please, sign in first")
    id = ret[0][0]
    c.execute("select id from friend where (id1=? and id2=?) or (id1=? and id2=?);", (id, uid, uid, id))
    ret = c.fetchall()
    c.close()
    if ret != []:
        return template('error', id=id, msg="User is already your friend :)")
    if id >= int(uid):
        return template('error', id=id, msg="You can't add this user to your friends")
    db.execute('insert into friend(id1, id2) values (?, ?);', (id, uid))
    db.commit()
    db.close()
    return redirect("/user?id=" + str(id))
    
 
@get('/signup')
def regget():
    TEMPLATES.clear()
    return template('signup', msg="")
 
@post('/signup')
def regpost():
    TEMPLATES.clear()
    db = sqlite3.connect('./facebear.db')
    c = db.cursor()
    name = request.forms.get('name')
    password = request.forms.get('password')
    cpassword = request.forms.get('cpassword')
    sex = request.forms.get('sex')
    about = request.forms.get('about')

    if password != cpassword:
        return template('signup', msg="Sorry, your passwords do not match")

    c.execute('select name from user where name=?;', (name,))
    row = c.fetchall()
    c.close()
    if row != []:
        return template('signup', msg='This user already exists')

    sid = randstring(10)
    db.execute('insert into user(name, password, sid, sex, about) values(?, ?, ?, ?, ?);', (name, sha1(password).hexdigest(), sid, sex, about))
    db.commit()
    response.set_cookie('sid', sid)
    c = db.cursor()
    c.execute('select id from user where sid=?', (sid,))
    row = c.fetchall()
    print row
    c.close()
    db.close()
    if row == []:
        return template('signup', msg='Internal error')
    id = row[0][0]
    return redirect("/user?id=" + str(id))

@get('/all')
def all():
    TEMPLATES.clear()
    db = sqlite3.connect('./facebear.db')
    c = db.cursor()
    sid = request.get_cookie("sid")
    c.execute("select id from user where sid=?;", (sid,))
    ret = c.fetchall()
    if ret == []:
        return template('signin', msg="Please, sign in first")
    id = ret[0][0]
    c.execute('select id, name from user')
    row = c.fetchall()
    if row == []:
        return template('signin', msg='Internal error')
    users = []
    for r in row:
        if r[0] != id:
            users.append(User(id=r[0], name=r[1]))
    return template('all', users=users, id=id)

@get('/send')
def getsend():
    TEMPLATES.clear()
    uid = int(request.query['id'])
    
    """Get current user id"""
    db = sqlite3.connect("facebear.db")
    c = db.cursor()
    sid = request.get_cookie("sid")
    c.execute("select id from user where sid=?;", (sid,))
    ret = c.fetchall()
    if ret == []:
        return template('signin', msg="Please, sign in first")
    id = ret[0][0]
    
    usr = User(id=uid)
    return template("send", id=id, user=usr)

@post("/send")
def postsend():
    TEMPLATES.clear()
    uid = int(request.query['id'])
    theme = request.forms.get('theme')
    msg = request.forms.get('msg')
    
    """Get current user id"""
    db = sqlite3.connect("facebear.db")
    c = db.cursor()
    sid = request.get_cookie("sid")
    c.execute("select id from user where sid=?;", (sid,))
    ret = c.fetchall()
    if ret == []:
        return template('signin', msg="Please, sign in first")
    id = ret[0][0]
    
    c.execute("select id from user where id=?;", (uid,))
    ret = c.fetchone()
    c.close()
    if not ret:
        return template('error', id=id, msg="No such user")

    db.execute("insert into message (fid, uid, msg, theme) values (?, ?, ?, ?);", (id, uid, msg, theme))
    db.commit()
    db.close() 
    return redirect("/user?id=" + str(id))

@get("/msgs")
def getmsgs():
    TEMPLATES.clear()
        
    """Get current user id"""
    db = sqlite3.connect("facebear.db")
    c = db.cursor()
    sid = request.get_cookie("sid")
    c.execute("select id from user where sid=?;", (sid,))
    ret = c.fetchall()
    if ret == []:
        return template('signin', msg="Please, sign in first")
    id = ret[0][0]

    c.execute("select mid, theme, fid from message where uid=?;", (id,))
    ret = c.fetchall()
    msgs = []
    for r in ret:
        c.execute("select name from user where id=?;", (r[2],))
        fname = c.fetchone()[0]
        msgs.append(Msg(mid=r[0], theme=r[1], fid=r[2], frm=fname))
    msgs.sort()
    return template('msgs', id=id, msgs=msgs)

@get("/view")
def getview():
    TEMPLATES.clear()
    """Get current user id"""
    db = sqlite3.connect("facebear.db")
    c = db.cursor()
    sid = request.get_cookie("sid")
    c.execute("select id from user where sid=?;", (sid,))
    ret = c.fetchall()
    if ret == []:
        return template('signin', msg="Please, sign in first")
    id = ret[0][0]


    mid = int(request.query['id'])
    c.execute("select msg from message where mid=?;", (mid,))
    msg = c.fetchone()[0]
    c.close()
    db.close()
    return template("view", id=id, msg=msg)
   

@get('/signout')
def signout():
    response.delete_cookie("sid")
    return redirect("/signin")

@route('/css/<filename>')
def server_css(filename):
    return static_file(filename, root='./css')

@route('/css/images/<filename>')
def server_css_images(filename):
    return static_file(filename, root='./css/images')

@route('/js/<filename>')
def server_js(filename):
    return static_file(filename, root='./js')

@route('/images/<filename>')
def server_images(filename):
    return static_file(filename, root='./images')
if __name__ == "__main__":
    run(host="0.0.0.0", port=8080)
    exit(0)
