<!DOCTYPE HTML>
<!--
	Minimaxing 3.0 by HTML5 UP
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>FaceBears</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<link href="http://fonts.googleapis.com/css?family=Ubuntu+Condensed" rel="stylesheet">
		<script src="js/jquery.min.js"></script>
		<script src="js/config.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/skel-panels.min.js"></script>
		<noscript>
			<link rel="stylesheet" href="css/skel-noscript.css" />
			<link rel="stylesheet" href="css/style.css" />
			<link rel="stylesheet" href="css/style-desktop.css" />
		</noscript>
		<!--[if lte IE 9]><link rel="stylesheet" href="css/ie9.css" /><![endif]-->
		<!--[if lte IE 8]><script src="js/html5shiv.js"></script><![endif]-->
	</head>
	<body>
	<!-- ********************************************************* -->
		<div id="header-wrapper">
			<div class="container">
				<div class="row">
					<div class="12u">
						
						<header id="header">
							<h1><a href="#" id="logo">FaceBears</a></h1>
							<nav id="nav">
								<a href="signin" class="current-page-item">Sign In</a>
								<a href="signup">Sign Up</a>
							</nav>
						</header>
					
					</div>
				</div>
			</div>
		</div>
		<div id="banner-wrapper">
			<div class="container">
				<div class="row">
					<div class="12u">
					
						<div id="banner">
							<h2>Welcome to the coolest social network!</h2>
							<span>hack the world with us</span>
						</div>
					</div>
				</div>
			</div> </div> <div id="main"> <div class="container">
                <div class="row">
                %if msg is not "":
                    <div class="msg" align="center" style="color: red; boarder 2px solid red; border-radius: 5px;">
                        <p>{{msg}}</p> 
                    </div>
                %end
                    <section class="center">
                    <div class="login-form">
                            <form method="POST" action="/signin">
                                <fieldset>
                                <div class="field">
                                    <input type="text" name="name" placeholder="Username">
                                </div>
                                <div class="field">
                                    <input type="password" name="password" placeholder="Password">
                                </div>
                                <button class="button" type="submit" style="padding:">Sign in</button>
                                </fieldset>
                            </form>
                    </div>
                    </section>
                </div>
			</div>
		</div>
		<div id="footer-wrapper">
			<div class="container">
				<div class="row">
					<div class="12u">

						<div id="copyright">
							&copy; FaceBears. All rights reserved. | Design: <a href="http://html5up.net">HTML5 UP</a>
						</div>

					</div>
				</div>
			</div>
		</div>
	<!-- ********************************************************* -->
	</body>
</html>
