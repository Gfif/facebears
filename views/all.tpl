<!DOCTYPE HTML>
<!--
	Minimaxing 3.0 by HTML5 UP
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>FaceBears</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<link href='http://fonts.googleapis.com/css?family=Ubuntu+Condensed' rel='stylesheet' type='text/css'>
		<script src="js/jquery.min.js"></script>
		<script src="js/config.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/skel-panels.min.js"></script>
		<noscript>
			<link rel="stylesheet" href="css/skel-noscript.css" />
			<link rel="stylesheet" href="css/style.css" />
			<link rel="stylesheet" href="css/style-desktop.css" />
		</noscript>
		<!--[if lte IE 9]><link rel="stylesheet" href="css/ie9.css" /><![endif]-->
		<!--[if lte IE 8]><script src="js/html5shiv.js"></script><![endif]-->
	</head>
	<body>
	<!-- ********************************************************* -->
		<div id="header-wrapper">
			<div class="container">
				<div class="row">
					<div class="12u">
						
						<header id="header">
							<h1><a href="#" id="logo">FaceBears</a></h1>
							<nav id="nav">
								<a href="/user?id={{id}}">My page</a>
								<a href="/msgs">My messages</a>
								<a href="/all" class="current-page-item">all users</a>
								<a href="/signout">sign out</a>
							</nav>
						</header>
					
					</div>
				</div>
			</div>
		</div>
		<div id="main">
			<div class="container">
				<div class="row main-row">
					<div class="12u">
						
						<section class="left-content">
							<h2>All users</h2>
                                        <ul class="link-list">
                                         %for u in users:
                                            <li><a href="/user?id={{u.id}}">{{u.name}}</a><a href="/add?id={{u.id}}" class="button" style="float: right; padding:3px; margin-left:30px;">add to friends</a><a href="/send?id={{u.id}}" class="button" style="float: right; padding:3px; margin-left:10px">send message</a></li>
                                         %end
										</ul>
						</section>
					</div>
				</div>
			</div>
		</div>
		<div id="footer-wrapper">
			<div class="container">
					<div class="12u">
						<div id="copyright">
							&copy; FaceBears. All rights reserved. | Design: <a href="http://html5up.net">HTML5 UP</a>
						</div>

					</div>
				</div>
			</div>
		</div>
	<!-- ********************************************************* -->
	</body>
</html>
