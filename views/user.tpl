<!DOCTYPE HTML>
<!--
	Minimaxing 3.0 by HTML5 UP
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>FaceBears</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<link href='http://fonts.googleapis.com/css?family=Ubuntu+Condensed' rel='stylesheet' type='text/css'>
		<script src="js/jquery.min.js"></script>
		<script src="js/config.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/skel-panels.min.js"></script>
		<noscript>
			<link rel="stylesheet" href="css/skel-noscript.css" />
			<link rel="stylesheet" href="css/style.css" />
			<link rel="stylesheet" href="css/style-desktop.css" />
		</noscript>
		<!--[if lte IE 9]><link rel="stylesheet" href="css/ie9.css" /><![endif]-->
		<!--[if lte IE 8]><script src="js/html5shiv.js"></script><![endif]-->
	</head>
	<body>
	<!-- ********************************************************* -->
		<div id="header-wrapper">
			<div class="container">
				<div class="row">
					<div class="12u">
						
						<header id="header">
							<h1><a href="#" id="logo">FaceBears</a></h1>
							<nav id="nav">
								<a href="/user?id={{id}}" class="current-page-item">My page</a>
								<a href="/msgs">my messages</a>
								<a href="/all">all users</a>
								<a href="/signout">sign out</a>
							</nav>
						</header>
					</div>
				</div>
			</div>
		</div>
		<div id="main">
			<div class="container">
				<div class="row main-row">
                %if msg is not None:
                    <div class="msg" align="center" style="color: red; boarder 2px solid red; border-radius: 5px;">
                        <p>{{msg}}</p> 
                    </div>
                %end
                %if user is not None: 
					<div class="4u">
						<section>
							<a href="#"><img src="images/{{user.sex}}.png" alt="" class="left" height="350" width="350"/></a>
						</section>
					    <br />
											</div>
					<div class="6u skel-cell-mainContent">
						<section class="right-content">
							<h3>{{user.name}}</h3>
                            <h1>About {{user.name}}</h1>
                            %if isfriend:
                                {{user.about}}
                            %else:
                                <p style="color: red;">You can't see information about this user, 'couse you are not friends yet :(</p>
                            %end
						</section>
					</div>
                    <div class="2u">
                        <section>
							<h2>{{user.name}}'s friends</h2>
							<div>
							    <ul class="link-list">
                                %if friends is not []:
                                %for f in friends:
									<li><a href="/user?id={{f.id}}">{{f.name}}</a></li>
                                %end
                                %else:
                                <p>No friends :(</p>
								</ul>
							</div>
						</section>
                    </div>
                %end
				</div>
			</div>
		</div>
		<div id="footer-wrapper">
			<div class="container">
				<div class="row">
					<div class="12u">

						<div id="copyright">
							&copy; FaceBears. All rights reserved. | Design: <a href="http://html5up.net">HTML5 UP</a>
						</div>

					</div>
				</div>
			</div>
		</div>
	<!-- ********************************************************* -->
	</body>
</html>
